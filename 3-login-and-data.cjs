const fs = require('fs');
const path = require('path');


// NOTE: Do not change the name of this file

// NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
function readFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.readFile(filePath, 'utf-8', function (error, data) {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
}

function createFile(fileName, fileData) {
    const filePath = path.join(__dirname, fileName);

    return new Promise(function (resolve, reject) {

        fs.writeFile(filePath, fileData, function (error) {
            if (error) {
                reject("File not created");
            } else {
                resolve(filePath);
            }
        });
    });
}

function deleteFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.unlink(filePath, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve(filePath);
            }
        });
    });
}


function createTwoFilesAndDeleteOneAfterAnother() {

    const file1 = createFile("file1.json", "3-login-and-dada");
    const file2 = createFile("file2.json", "3-login-and-dada");

    const filesTOdelete = [file1, file2];

    setTimeout(function () {
        filesTOdelete.map(function (currentFile) {
            currentFile.then(function (filePath) {
                deleteFile(filePath);
            })
                .catch(function (error) {
                    console.log(error);
                });
        });

    }, 2 * 1000);

}




// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining

function readLipsumDataCopyToNewFileDeleteOriginal() {

    const pathForLipsumOriginalFile = path.join(__dirname, "lipsum.txt");
    const nameForNewFile = "newLipsum.txt"

    readFile(pathForLipsumOriginalFile)
        .then(function (lipsumData) {
            return createFile(nameForNewFile, lipsumData);
        })
        .then(function (filePath) {
            console.log(`FIle ${filePath} Created`);
        })
        .then(function () {
            return deleteFile(pathForLipsumOriginalFile)
        })
        .then(function (filePath) {
            console.log(`File ${filePath} deleted successfully`);
        })
        .catch(function (error) {
            console.log(error);
        })

}

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}




// Q3.
// Use appropriate methods to 
// A. login with value 3 and call getData once login is successful

const user = {
    name: "Shiva",
    profile: "SDE"
}

function loginWithValue3() {
    login(user, 3).then(function (user) {
        console.log(`Login Successfull .`);
        return getData();
    })
        .then(function (userData) {
            console.log(userData);
        })
        .catch(function (error) {
            console.log("Login failed user not found");
            console.log(error);
        })
}

// loginWithValue3();
// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"
//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.
//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise
const logFilePath = path.join(__dirname, "logData.txt");

function logData(user, activity) {
    // use promises and fs to save activity in some file
    const fileData = JSON.stringify(user) + " " + JSON.stringify(activity) + " " + JSON.stringify(Date.now());
    return new Promise(function (resolve, reject) {
        fs.appendFile(logFilePath, fileData + "\n", function (error) {
            if (error) {
                reject(error);
            } else {
                resolve(fileData);
            }
        })
    })
}

function logicForLogData(user, val) {
    login(user, 2)
        .then(function (user) {
            console.log("Login Successful");
            return logData(user, "Login success");
        })
        .catch(function (error) {
            logData(user, "Login failed")
            console.log(error);
        })
}

logicForLogData();